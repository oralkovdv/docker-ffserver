import os
import re
import sys
import yaml
import argparse
import subprocess

STREAM_TEMPLATE = """
<Stream {name}>
    Format rtp
    File "{source}"
</Stream>

"""

FFSERVER_CONFIG_PATH = '/etc/ffserver.conf'
CONVERT_COMMAND = 'ffmpeg -i {source} -c:v libx264 -ar 22050 -crf 28 {result}'
EXTENSION_RE = r'\..+?$'


def convert2flv(source, result):
    cmd = CONVERT_COMMAND.format(source=source, result=result)
    process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    print(output)
    print(error)


def main(videos_path):
    with open(videos_path, 'r') as f:
        try:
            conf = yaml.safe_load(f)
            conf = conf['videos']
        except yaml.YAMLError as ex:
            print(ex)
            sys.exit(-1)
        except KeyError as ex:
            print(f"No {ex} in videos config file")

    with open(FFSERVER_CONFIG_PATH, 'a') as f:
        for video in conf:
            source = video['source']
            extension = re.findall(EXTENSION_RE, source)[0]

            flv_path = source.replace(extension, '.flv')
            video['source'] = flv_path
            if not os.path.exists(flv_path):
                convert2flv(source, flv_path)
            f.write(STREAM_TEMPLATE.format(**video))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Creates ffserver.conf')
    parser.add_argument('-v',
                        type=str,
                        help='path to videos config file',
                        dest='videos_path',
                        default='./videos.yaml')

    args = parser.parse_args()
    main(args.videos_path)
