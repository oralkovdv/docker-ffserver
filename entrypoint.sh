#!/usr/bin/env bash

rm /etc/ffserver.conf
cp /src/ffserver.conf /etc/ffserver.conf

python3.7 /src/config.py -v /conf/videos.yaml

ffserver -d
